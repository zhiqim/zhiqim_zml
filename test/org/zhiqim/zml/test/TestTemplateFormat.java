/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.test;

import org.zhiqim.zml.Zmls;

public class TestTemplateFormat
{
    public static void main(String[] args) throws Exception
    {
        String content = "1111\r\n${a}\r\n${a}\r\n <a class=\"f-font \r\n    <#function abcdeff()>\r\n\r\n    f-add\r\n\r\n    </#function>\r\n    2222";
        System.out.println(content);
        System.out.println("=================================================");
        String result = Zmls.parse(content, "aa", "aa");
        System.out.println(result);
        System.out.println("=================================================");
        String result2 = Zmls.parse(content, "a", "1");
        System.out.println("=================================================");
        System.out.println(result2);
    }
}
