/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.test;

import org.zhiqim.kernel.model.maps.HashMapSO;
import org.zhiqim.kernel.model.maps.MapSO;
import org.zhiqim.kernel.util.Files;
import org.zhiqim.zml.Zmls;

public class TestOne
{
    public static void main(String[] args) throws Exception
    {
        String content = Files.readUTF8("./resource/one.zml");
        
        MapSO variableMap = new HashMapSO();
        variableMap.put("name", "测试表达式");
        variableMap.put("loginUrl", "/login.htm");
        variableMap.put("styleColor", "f-blue");
        variableMap.put("hasAutoComplete", false);
        
        String result = Zmls.parse(content, variableMap);
        System.out.println(result);
    }
}
