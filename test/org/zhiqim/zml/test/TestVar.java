/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.test;

import org.zhiqim.kernel.Zhiqim;
import org.zhiqim.kernel.annotation.AnAlias;
import org.zhiqim.kernel.annotation.AnNew;
import org.zhiqim.kernel.model.lists.ArrayListS;
import org.zhiqim.kernel.model.maps.HashMapSO;
import org.zhiqim.kernel.model.maps.MapSO;
import org.zhiqim.kernel.util.Files;
import org.zhiqim.kernel.util.Systems;
import org.zhiqim.zml.Zmls;

@AnAlias("TestVar")
@AnNew
public class TestVar
{
    public static void main(String[] args) throws Exception
    {
        Zhiqim.loadClassAliasName(Systems.getClassPathList());
        
        String content = Files.readUTF8("./resource/var.zml");
        
        MapSO variableMap = new HashMapSO();
        variableMap.put("ArrayListS", ArrayListS.class);
        
        String result = Zmls.parse(content, variableMap);
        System.out.println(result);
    }
}
