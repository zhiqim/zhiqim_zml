/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

/**
 * ZhiqimML加载接口定义
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public interface ZmlLoader extends ZmlConstants
{
    /** 获取上下文环境路径 */
    public String getContextPath();
    
    /**
     * 判断ZML是否存在
     * 
     * @param path          ZML路径
     * @return              是否存在true/false
     * @throws              可能的异常
     */
    public boolean hasZml(String path) throws Exception;
    
    /**
     * 加载ZML接口
     * 
     * @param path          相对于根配置的路径，如/zview/include.zml
     * @return              返回Zml对象
     * @throws              可能的异常
     */
    public Zml loadZml(String path) throws Exception;
    
    /**
     * 获取ZML资源最后更新时间
     * 
     * @param path          相对于根配置的路径，如/zview/include.zml
     * @return long         最后修改时间
     * @throws              可能的异常
     */
    public long getLastModified(String path) throws Exception;
    
    /** 
     * 设置ZML缓存最长空闲时长
     * 
     * @param maxIdleTime   最长空闲时长，单位：秒
     */
    public void setMaxIdleTime(int maxIdleTime);
    
    /**
     * 设置ZML缓存最长保持时长
     * 
     * @param maxKeepTime   最长保持时长，单位：秒
     */
    public void setMaxKeepTime(int maxKeepTime);
}
