/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.expression.operator._Or;
import org.zhiqim.zml.statement._Var;

/**
 * ZhiqimML运算符表达式基类<br><br>
 * 
 * 以下为Java操作符优先级排序<br>
 *  优先级                     操作                                   结合性]<br>
 *  1       后缀运算符         [] . () (函数呼叫)                      从左到右<br>
 *  2       单目运算符         ! ~ ++ -- +(单操作数) –(单操作数)       从右到左<br>
 *  3       创建               new                                    从左到右<br>
 *  4       乘除取模           * / %                                  从左到右<br>
 *  5       加减               + -                                    从左到右<br>
 *  6       移位               << >> >>>                              从左到右<br>
 *  7       关系               < <= > >= instanceof                   从左到右<br>
 *  8       相等               == !=                                  从左到右<br>
 *  9       按位与             &                                      从左到右<br>
 *  10      按位异或           ^                                      从左到右<br>
 *  11      按位或             |                                      从左到右<br>
 *  12      逻辑与             &&                                     从左到右<br>
 *  13      逻辑或             ||                                     从左到右<br>
 *  14      条件               ? :                                    从右到左<br>
 *  15      赋值               = += -= *= /= %= ^= <<= >>= >>>=       从右到左 <br>
 *
 * ZhiqimML操作符只实现如下操作符，共22个(其中1个赋值/20个运算表达式)：<br>
 *  1       后缀运算符          [ ] . ( )(方法调用或表达式括号)         从左到右<br>
 *  2       单目运算符          ! –(数值取负)  ..(数值最小最大值)  　   从右到左<br>
 *  3       创建                new                                   从左到右<br>
 *  4       乘除取模            * / %                                 从左到右<br>
 *  5       加减                + -                                   从左到右<br>
 *  6       关系                lt lte gt gte                         从左到右<br> 
 *  7       相等                == !=                                 从左到右<br>
 *  8       逻辑与              &&                                    从左到右<br>
 *  9       逻辑或              ||                                    从左到右<br>
 *  10      赋值                =                                     从右到左 <br>
 *  
 * @see _Var 其中赋值操作符(=)只存在于定义中<br>
 * @see _Or     其他操作符都可能出现在表达式中，以逻辑或 优化级最低，默认为_Or，依优化级从低到高依次判断<br>
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public abstract class Operator implements Expression
{

}
