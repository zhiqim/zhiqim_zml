/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;
import org.zhiqim.zml.expression.primitive._Decimal;
import org.zhiqim.zml.expression.primitive._Integer;

/**
 * 取负运算(-)
 * 
 * @see _Subtraction 和减号 运算符相同，要注意区分
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Negative extends Operator
{
    private final Expression obj;
    
    public _Negative(Expression obj)
    {
        this.obj = obj;
    }
    
    @Override
    public int getType()
    {
        return NEGATIVE;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object value = obj.build(variableMap);
        if (value instanceof Integer)
        {
            int v = (Integer)value;
            return -v;
        }
        else if (value instanceof Long)
        {
            long v = (Long)value;
            return -v;
        }
        else if (value instanceof Float)
        {
            float v = (Float)value;
            return -v;
        }
        else if (value instanceof Double)
        {
            double v = (Double)value;
            return -v;
        }
        
        throw new ExpressionException("取负表达式{"+this+"}格式不正常，要求结果不是整数或小数");
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder("-").append(obj).toString() ;
    }
    
    public Number getValue() throws ExpressionException
    {
        if (obj instanceof _Integer)
        {
            _Integer value = (_Integer)obj;
            return -value.getLong();
        }
        else if (obj instanceof _Decimal)
        {
            _Decimal value = (_Decimal)obj;
            return -value.getDouble();
        }
        else if (obj instanceof _Bracket)
        {
            Expression expression = ((_Bracket)obj).getExpression();
            if (expression instanceof _Integer)
            {
                _Integer value = (_Integer)obj;
                return -value.getLong();
            }
            else if (expression instanceof _Decimal)
            {
                _Decimal value = (_Decimal)obj;
                return -value.getDouble();
            }
        }
        
        throw new ExpressionException("[逻辑取负运算符]解析时异常，对象不是数值型["+obj+"]");
    }
}
