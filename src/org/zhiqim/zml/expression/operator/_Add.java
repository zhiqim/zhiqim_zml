/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.kernel.util.Types;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 加法运算(+)<br><br>
 *
 * 1、数值型相加<br>
 * 2、字符串相连<br>
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Add extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _Add(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return ADD;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        Object next = right.build(variableMap);
        if (privous instanceof String || next instanceof String)
        {//有一个是字符串则认为是字符串相连
            String str1 = String.valueOf(privous);
            String str2 = String.valueOf(next);
            return str1 + str2;
        }
        else if ((!Types.isNumber(privous) && !Types.isChar(privous))
            || (!Types.isNumber(next) && !Types.isChar(next)))
        {//不是整数和小数
            throw new ExpressionException("加法表达式{"+this+"}，格式不正确，出现加数不是数值型和字符串");
        }
        else
        {
            if (Types.isInteger(privous))
            {//整数
                long add1 = ((Number)privous).longValue();
                if (Types.isInteger(next))
                {
                    long add2 = ((Number)next).longValue();
                    return add1 + add2;
                }
                else if (Types.isDecimal(next))
                {
                    double add2 = ((Number)next).doubleValue();
                    return add1 + add2;
                }
                else
                {
                    char add2 = (Character)next;
                    return add1 + add2;
                }
            }
            else if (Types.isDecimal(privous))
            {//小数
                double add1 = (Double)privous;
                if (Types.isInteger(next))
                {
                    long add2 = ((Number)next).longValue();
                    return add1 + add2;
                }
                else if (Types.isDecimal(next))
                {
                    double add2 = ((Number)next).doubleValue();
                    return add1 + add2;
                }
                else
                {
                    char add2 = (Character)next;
                    return add1 + add2;
                }
            }
            else
            {//字符
                char add1 = (Character)privous;
                if (Types.isInteger(next))
                {
                    long add2 = ((Number)next).longValue();
                    return add1 + add2;
                }
                else if (Types.isDecimal(next))
                {
                    double add2 = ((Number)next).doubleValue();
                    return add1 + add2;
                }
                else
                {
                    char add2 = (Character)next;
                    return add1 + add2;
                }
            }
        }
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append(" + ").append(right).toString() ;
    }
}
