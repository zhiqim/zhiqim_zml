/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ExpressionParser;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 小于比较(<)，对_Lt左右两值进行小于比较操作
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _LessThen extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _LessThen(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return LTHEN;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        Object next = right.build(variableMap);
        return ExpressionParser.compare(privous, next, this);
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append(" lt ").append(right).toString() ;
    }
}
