/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.kernel.util.Types;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 整数数组，双点号运算符(..)
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _IntegerArray extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _IntegerArray(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return INTEGER_ARR;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        if (!Types.isInteger(privous))
            throw new ExpressionException("整数数组表达式{"+this+"}格式不正常，要求结果不是整数");
            
        Object next = right.build(variableMap);
        if (!Types.isInteger(next))
            throw new ExpressionException("整数数组表达式{"+this+"}格式不正常，要求结果不是整数");
        
        int v1 = ((Number)privous).intValue();
        int v2 = ((Number)next).intValue();
        if (v1 > v2)
            return new int[0];
        
        int[] ints = new int[v2 - v1 + 1];
        for (int i=0;i<ints.length;i++)
        {
            ints[i] = v1 + i;
        }
        return ints;
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append("..").append(right).toString() ;
    }
}
