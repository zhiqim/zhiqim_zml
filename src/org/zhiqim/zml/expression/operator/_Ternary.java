/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 三目运算（?a:b）
 *
 * @version v1.0.0 @author zouzhigang 2018-1-2 新建与整理
 */
public class _Ternary extends Operator
{
    private Expression obj;
    private Expression first;
    private Expression second;
    
    public _Ternary(Expression condition, Expression first, Expression second)
    {
        this.obj = condition;
        this.first = first;
        this.second = second;
    }
    
    @Override
    public int getType()
    {
        return TERNARY;
    }

    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object value = obj.build(variableMap);
        if (value == null)
        {//为null时，采用js的规则，判断为false
            return second.build(variableMap);
        }
        
        if (value instanceof Boolean)
        {//值为boolean型
            return ((Boolean)value)?first.build(variableMap):second.build(variableMap);
        }
        
        //非null且非boolean的不支持
        throw new ExpressionException("三目表达式{"+this+"}格式不正常，条件不是布尔型");
    }
}
