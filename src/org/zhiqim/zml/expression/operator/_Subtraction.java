/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.kernel.util.Types;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 减法运算(-)，支持整数、小数和字符相减<br><br>
 *
 * 1、整数相减  56-23,46-21.12,102-'A'<br>
 * 2、小数相减  56.6-23,46.9-21.12,102.2-'A'<br>
 * 3、字符相减  'A'-23,'a'-21.12,'Z'-'A'<br>
 * @see _Negative 和逻辑取负 运算符相同，要注意区分
 * 
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Subtraction extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _Subtraction(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return SUBTRACTION;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        Object next = right.build(variableMap);
        
        if ((!Types.isNumber(privous) && !Types.isChar(privous))
            || (!Types.isNumber(next) && !Types.isChar(next)))
        {//不是整数和小数
            throw new ExpressionException("减法表达式{"+this+"}，格式不正确，减数或被减数不是数值型");
        }
        
        if (Types.isInteger(privous))
        {//整数
            long minuend = ((Number)privous).longValue();
            if (Types.isInteger(next))
            {
                long subtrahend = ((Number)next).longValue();
                return minuend - subtrahend;
            }
            else if (Types.isDecimal(next))
            {
                double subtrahend = ((Number)next).doubleValue();
                return minuend - subtrahend;
            }
            else
            {
                char subtrahend = (Character)next;
                return minuend - subtrahend;
            }
        }
        else if (Types.isDecimal(privous))
        {//小数
            double minuend = ((Number)privous).doubleValue();
            if (Types.isInteger(next))
            {
                long subtrahend = ((Number)next).longValue();
                return minuend - subtrahend;
            }
            else if (Types.isDecimal(next))
            {
                double subtrahend = ((Number)next).doubleValue();
                return minuend - subtrahend;
            }
            else
            {
                char subtrahend = (Character)next;
                return minuend - subtrahend;
            }
        }
        else
        {//字符
            char minuend = (Character)privous;
            if (Types.isInteger(next))
            {
                long subtrahend = ((Number)next).longValue();
                return minuend - subtrahend;
            }
            else if (Types.isDecimal(next))
            {
                double subtrahend = ((Number)next).doubleValue();
                return minuend - subtrahend;
            }
            else
            {
                char subtrahend = (Character)next;
                return minuend - subtrahend;
            }
        }
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append(" - ").append(right).toString() ;
    }
}
