/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.kernel.util.Types;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 乘法运算符(*)
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Multiplication extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _Multiplication(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return MULTIPLICATION;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        Object next = right.build(variableMap);
        
        if (!Types.isNumber(privous) || !Types.isNumber(next))
        {//不是整数和小数
            throw new ExpressionException("乘法表达式{"+this+"}，格式不正确，出现乘数不是数值型");
        }
        
        Number p = (Number)privous;
        Number n = (Number)next;
        
        if (Types.isInteger(privous))
        {//整数
            if (Types.isInteger(next))
                return p.longValue() * n.longValue();
            else
                return p.longValue() * n.doubleValue();
        }
        else
        {//小数
            if (Types.isInteger(next))
                return p.doubleValue() * n.longValue();
            else
                return p.doubleValue() * n.doubleValue();
        }
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append(" * ").append(right).toString() ;
    }
}
