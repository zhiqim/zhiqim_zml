/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import java.util.ArrayList;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ExpressionParser;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 括号运算()
 *
 * @see _Method 括号操作符，和方法操作符不同的是内部是表达式，方法操作符括号内为参数
 * 
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Bracket extends Operator
{
    private Expression _expression;                    //编译后的表达式列表
    
    private transient ArrayList<Expression> eList;     //预编译的表达式列表
    
    public _Bracket()
    {
        eList = new ArrayList<Expression>();
    }
    
    @Override
    public int getType()
    {
        return BRACKET;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        return _expression.build(variableMap);
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder("(").append(_expression).append(")").toString() ;
    }
    
    public void addExpression(Expression expression)
    {
        eList.add(expression);
    }
    
    public ArrayList<Expression> getExpressionList()
    {
        return eList;
    }
    
    public Expression getExpression()
    {
        return _expression;
    }
    
    public void parseBracket() throws ExpressionException
    {
        //左右括号去除
        eList.remove(0);
        eList.remove(eList.size()-1);
        
        ExpressionParser.parse_Operator(eList);
        
        if (eList.size() > 1)
            throw new ExpressionException("括号表达式存在无法处理的表达式");
        
        _expression = eList.get(0);
        eList.clear();eList = null;
    }
}
