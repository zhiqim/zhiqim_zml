/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.operator;

import org.zhiqim.kernel.util.Types;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;

/**
 * 不等比较(!=)
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _EqualNot extends Operator
{
    private final Expression left;
    private final Expression right;
    
    public _EqualNot(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public int getType()
    {
        return EQUAL_NOT;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        Object privous = left.build(variableMap);
        Object next = right.build(variableMap);
        
        if (privous == null || next == null)
        {//出现一个为null
            return (privous == null)?(next != null):(next == null);
        }
        
        if (privous instanceof String || next instanceof String)
        {//出现一个为字符串
            return !String.valueOf(privous).equals(String.valueOf(next));
        }
        
        //判断是否数值型，不是数值型，进行对象相等判断
        if (!Types.isNumber(privous))
            return !privous.equals(next);
        
        //是数值型，进行数值比较
        
        //next不是数值型
        if (!Types.isNumber(next))
            return true;
        
        if (Types.isInteger(privous))
        {//整数
            if (!Types.isInteger(next))
                return true;
            
            return ((Number)privous).longValue() != ((Number)next).longValue();
        }
        else
        {//小数
            if (!Types.isDecimal(next))
                return true;
            
            return ((Number)privous).doubleValue() != ((Number)next).doubleValue();
        }
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append(left).append(" != ").append(right).toString() ;
    }
}
