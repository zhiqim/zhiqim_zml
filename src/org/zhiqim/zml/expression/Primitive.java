/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression;

import org.zhiqim.zml.Expression;
import org.zhiqim.zml.expression.primitive._Boolean;
import org.zhiqim.zml.expression.primitive._Char;
import org.zhiqim.zml.expression.primitive._Decimal;
import org.zhiqim.zml.expression.primitive._Integer;
import org.zhiqim.zml.expression.primitive._Null;
import org.zhiqim.zml.expression.primitive._String;
import org.zhiqim.zml.expression.primitive._Variable;

/**
 * 原生类型对象表达式基类
 * 
 * @see _Null       原生的null类型，表示null
 * @see _String     字符串原型，双引号""之内的内容
 * @see _Char       字符原型，单引号''之内的一个字符
 * @see _Boolean    为true/false的两个值
 * @see _Integer    整数原型，非零开头全数字组成
 * @see _Decimal    小数原型，零点开头接全数字，或非零开头后接一个点和数字
 * @see _Variable   变量原型，字母或下划线开头，后面接字母、下划线和数字
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public abstract class Primitive implements Expression
{

}
