/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.primitive;

import org.zhiqim.kernel.Z;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Primitive;

/**
 * 类原型，如FTable.class
 *
 * @version v1.0.0 @author zouzhigang 2016-6-27 新建与整理
 */
public class _Class extends Primitive
{
    private String value;
    
    public _Class(String value)
    {
        this.value = value;
    }

    @Override
    public int getType()
    {
        return CLASS;
    }

    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        String className = Strings.trimRight(value, ".class");
        return Z.cls().forName(className);
    }

    @Override
    public String toString()
    {
        return "class";
    }
    
    public String getValue()
    {
        return value;
    }
    
    public void setValue(String value)
    {
        this.value = value;
    }
}
