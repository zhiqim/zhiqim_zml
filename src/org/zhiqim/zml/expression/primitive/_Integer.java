/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.primitive;

import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Primitive;

/**
 * 整数原型，byte/short/int/long，非零数字开头数字连贯出现"^[1-9]\\d*$"<br><br>
 * 但是实际中可能出现0123，这种情况，和点号一起组合成_Decimal
 *
 * @see Byte        一个字节
 * @see Short       二个字节
 * @see Integer     四个字节
 * @see Long        八个字节
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Integer extends Primitive
{
    private final String value;
    
    public _Integer(String value)
    {
        this.value = value;
    }
    
    @Override
    public int getType()
    {
        return NUMBERIC;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        return Long.parseLong(value);
    }
    
    @Override
    public String toString()
    {
        return value;
    }

    public long getLong()
    {
        return Long.parseLong(value);
    }
    
    public int getInt()
    {
        return Integer.parseInt(value);
    }
    
    public short getShort()
    {
        return Short.parseShort(value);
    }
    
    public byte getByte()
    {
        return Byte.parseByte(value);
    }
}
