/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.primitive;

import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Primitive;
import org.zhiqim.zml.expression.operator._Negative;

/**
 * 小数原型，float/double，零点开头后面为数字，或非零数字开头数字和一个点号连贯出现"^(0|[1-9]\\d*)(\\.\\d+)?$"
 * 
 * @see _Negative  逻辑取负，如果前面有-号，则为负小数原型
 * 
 * @see Float       四个字节
 * @see Double      八个字节
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Decimal extends Primitive
{
    private final String value;
    
    public _Decimal(String value)
    {
        this.value = value;
    }
    
    @Override
    public int getType()
    {
        return DECIMAL;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        return Double.parseDouble(value);
    }
    
    @Override
    public String toString()
    {
        return value;
    }
    
    public double getDouble()
    {
        return Double.parseDouble(value);
    }
    
    public float getFloat()
    {
        return Float.parseFloat(value);
    }
}
