/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.expression.primitive;

import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Primitive;

/**
 * 字符串原型，格式为""或''之间的内容，注意转义字符
 * 1、"/>"要求使用&sgt;表示
 * 2、"}"要求使用&ccb;表示
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _String extends Primitive
{
    private final String value;
    
    public _String(String value)
    {
        this.value = processReplace(value);
    }
    
    @Override
    public int getType()
    {
        return STRING;
    }
    
    @Override
    public Object build(ZmlVariable variableMap) throws ExpressionException
    {
        return value.replaceAll("&#x7d;", "}");
    }
    
    @Override
    public String toString()
    {
        return new StringBuilder().append("\"").append(value).append("\"").toString();
    }
    
    /** 对字符串作处理 */
    private String processReplace(String str)
    {
        if (str == null)
            return null;
        
        if (str.length() >= 2 && str.startsWith("\"") && str.endsWith("\""))
        {//有双引号的两边删除
            str = str.substring(1, str.length()-1);
        }
        else if (str.length() >= 2 && str.startsWith("\'") && str.endsWith("\'"))
        {//有单引号的两边删除
            str = str.substring(1, str.length()-1);
        }
        
        //再对"}"和"/>"两种作替换
        str = str.replaceAll("&sgt;", "/>");
        str = str.replaceAll("&ccb;", "}");
        
        return str;
    }
}
