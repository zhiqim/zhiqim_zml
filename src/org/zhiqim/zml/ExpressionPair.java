/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import org.zhiqim.zml.expression.symbol._Ampersand;
import org.zhiqim.zml.expression.symbol._Dot;
import org.zhiqim.zml.expression.symbol._Equal;
import org.zhiqim.zml.expression.symbol._Exclamation;
import org.zhiqim.zml.expression.symbol._VerticalBar;

/**
 * 定义需要配对标点符号接口，共5个
 * 
 * @see _Ampersand      连接符&，由&&组成逻辑与_And
 * @see _VerticalBar    竖杠符|，由||组成逻辑或_Or
 * @see _Dot            点号符.，由..组成整数数组_IntegerArray
 * @see _Equal          等号符=，由==组成恒等_EqualEqual
 * @see _Exclamation    感叹号!，由!=组成不等_EqualNot
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public interface ExpressionPair
{
    /** 是否已配对 */
    public boolean isPaired();
    
    /** 设置已配对 */
    public void setPaired();
}
