/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import org.zhiqim.kernel.constants.CodeConstants;

/**
 * ZhiqimML常量定义
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public interface ZmlConstants extends CodeConstants
{
    /********************************************************************/
    //ZML文件缺省支持的模式
    /********************************************************************/
    
    public static final String[] ZML_PATTERN_DEFAULT    = {"*.zml", "*.htm"};
    
    /********************************************************************/
    //ZML语句定义
    /********************************************************************/

    public static final String TAG_END                  = ">";
    
    //ZML语句
    public static final String ECHO                     = "${}";
    public static final String CALL                     = "@{}";
    public static final String FORMAT                   = "#{}";
    public static final String COMMENT                  = "<#---->";
    public static final String INCLUDE                  = "<#include>";
    public static final String INTERCEPTOR              = "<#interceptor>";
    public static final String FUNCTION                 = "<#function>";
    public static final String VAR                      = "<#var>";
    public static final String DEF                      = "<#def>";
    public static final String IF                       = "<#if>";
    public static final String ELSEIF                   = "<#elseif>";
    public static final String FOR                      = "<#for>";
    public static final String RETURN                   = "<#return>";
    public static final String BREAK                    = "<#break/>";
    public static final String CONTINUE                 = "<#continue/>";
    
    public static final String ECHO_BEGIN               = "${";
    public static final String ECHO_END                 = "}";
    public static final String CALL_BEGIN               = "@{";
    public static final String CALL_END                 = "}";
    public static final String FORMAT_BEGIN             = "#{";
    public static final String FORMAT_END               = "}";
    public static final String COMMENT_BEGIN            = "<#--";
    public static final String COMMENT_END              = "-->";
    public static final String INCLUDE_BEGIN            = "<#include ";
    public static final String INCLUDE_END              = "/>";
    public static final String INTERCEPTOR_BEGIN        = "<#interceptor ";
    public static final String INTERCEPTOR_END          = "/>";
    public static final String FUNCTION_BEGIN           = "<#function ";
    public static final String FUNCTION_END             = "</#function>";
    public static final String VAR_BEGIN                = "<#var ";
    public static final String VAR_END                  = "/>";
    public static final String DEF_BEGIN                = "<#def ";
    public static final String DEF_END                  = "/>";
    public static final String IF_BEGIN                 = "<#if ";
    public static final String IF_ELSEIF                = "<#elseif ";
    public static final String IF_ELSE                  = "<#else>";
    public static final String IF_END                   = "</#if>";
    public static final String FOR_BEGIN                = "<#for ";
    public static final String FOR_SEPARATOR            = ":";
    public static final String FOR_END                  = "</#for>";
    public static final String RETURN_BEGIN             = "<#return";
    public static final String RETURN_END               = "/>";
    public static final String BREAK_BEGIN              = "<#break";
    public static final String BREAK_END                = "/>";
    public static final String CONTINUE_BEGIN           = "<#continue";
    public static final String CONTINUE_END             = "/>";
    
    /********************************************************************/
    //ZML表达式定义
    /********************************************************************/
    
    //原型-最基本
    public static final int NULL                        = 0;//null
    public static final int STRING                      = 1;//字符串
    public static final int CHAR                        = 2;//字符
    public static final int NUMBERIC                    = 3;//整数(数值型，允许前面是0开头)
    public static final int VARIABLE                    = 4;//变量
    public static final int BOOLEAN                     = 5;//布尔
    public static final int CLASS                       = 6;//类结构
    
    //原型-扩展
    public static final int DECIMAL                     = 7;//小数
    
    
    //符号，从变量中转化
    public static final int GT                          = 11;
    public static final int GTE                         = 12;
    public static final int LT                          = 13;
    public static final int LTE                         = 14;
    
    //符号，单标点13个
    public static final int BRACKET_LEFT                = '(';//(40
    public static final int BRACKET_RIGHT               = ')';//)41
    public static final int ASTERISK                    = '*';//*42
    
    public static final int BRACKET_SQUARE_LEFT         = '[';//[91
    public static final int BRACKET_SQUARE_RIGHT        = ']';//]93
    
    public static final int COMMA                       = ',';//,44
    public static final int DOT                         = '.';//.46
    public static final int EQUAL                       = '=';//=61
    public static final int EXCLAMATION                 = '!';//!33
    public static final int MINUS                       = '-';//-45
    public static final int PERCENT                     = '%';//%37
    public static final int PLUS                        = '+';//+43
    public static final int SLASH                       = '/';///47
    public static final int QUESTION                    = '?';//?63
    public static final int COLON                       = ':';//:58
    
    //符号，双标点2个
    public static final int CONNECTOR                   = '&';//&&38
    public static final int VERTICAL                    = '|';//||124
    
    //符号，标点扩展
    public static final int INEQUAL                     = 3361;//!=
    public static final int DOTDOT                      = 4646;//..
    public static final int TERNARY                     = 6358;//?:
    
    //操作符
    public static final int BRACKET                     = 4041;//()括号
    
    public static final int NEW                         = 110101119;//new，实例化newInstance
    public static final int METHOD                      = 40410;//()0，方法
    public static final int PROPERTY                    = 460;//.0，属性
    public static final int INDEXABLE                   = 9193;//[]，可索引对象(含数组索引、列表索引和MAP取值)

    
    public static final int ADD                         = 430;//+0加法
    public static final int NEGATIVE                    = 451;//-1取负
    public static final int SUBTRACTION                 = 452;//-2减法
    public static final int MULTIPLICATION              = 420;//*0乘法
    public static final int DIVISION                    = 470;///0除法
    public static final int MODULUS                     = 370;//%0取模
    
    public static final int GTHEN                       = 110;//大于
    public static final int GTEQUAL                     = 120;//大于等于
    public static final int LTHEN                       = 130;//小于
    public static final int LTEQUAL                     = 140;//小于等于
    
    public static final int EQUAL_EQUAL                 = 61610;//恒等
    public static final int EQUAL_NOT                   = 33610;//不等
    
    public static final int AND                         = 3800;//逻辑与
    public static final int OR                          = 1240;//逻辑或
    public static final int NOT                         = 3300;//取反
    
    public static final int INTEGER_ARR                 = 46460;//整型数组
    
    //标点操作符
    public static final String _BRACKET_LEFT            = "(";
    public static final String _BRACKET_RIGHT           = ")";
    public static final String _BRACKET_BOTH            = "()";
    public static final String _ASTERISK                = "*";
    
    public static final String _BRACKET_SQUARE_LEFT     = "[";
    public static final String _BRACKET_SQUARE_RIGHT    = "]";
    
    public static final String _COMMA                   = ",";
    public static final String _DOT                     = ".";
    public static final String _CONNECTOR               = "&&";
    public static final String _EQUAL                   = "=";
    public static final String _EXCLAMATION             = "!";
    public static final String _MINUS                   = "-";
    public static final String _PERCENT                 = "%";
    public static final String _PLUS                    = "+";
    public static final String _SLASH                   = "/";
    public static final String _VERTICAL                = "|";
    
    //标点扩展
    public static final String _DOTDOT                  = "..";
    public static final String _INEQUAL                 = "!=";
    public static final String _EQUALEQUAL              = "==";
}
