/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.loader;

import org.zhiqim.kernel.util.Files;
import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.zml.Zml;
import org.zhiqim.zml.ZmlCache;
import org.zhiqim.zml.ZmlEngine;
import org.zhiqim.zml.ZmlLoader;

/**
 * ZML文件加载器，指定文件根目录，传入相对目录即可
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class FileZmlLoader implements ZmlLoader
{
    private final ZmlEngine engine;          //ZML引擎
    private final ZmlCache cache;            //ZML缓存
    private final FileZmlWatcher watcher;    //ZML文件监视
    
    private String contextPath;
    
    /**
     * ZML文件加载器
     * 
     * @param engine        ZML引擎
     * @param contextPath   ZML前缀
     */
    public FileZmlLoader(ZmlEngine engine, String contextPath)
    {
        this.engine = engine;
        this.contextPath = Files.toLinuxPath(contextPath);
        
        this.cache = new ZmlCache(engine.getMaxIdleTime(), engine.getMaxIdleTime());
        this.watcher = new FileZmlWatcher(engine, this.contextPath); 
    }
    
    /** 获取上下文环境路径 */
    public String getContextPath()
    {
        return contextPath;
    }
    
    @Override
    public boolean hasZml(String path) throws Exception
    {
        return getLastModified(path) != -1;
    }
    
    @Override
    public Zml loadZml(String path) throws Exception
    {
        long lastModified = watcher.getLastModified(path);
        if (lastModified == -1)
        {//文件不存在删除缓存返回null
            cache.remove(path);
            return null;
        }
        
        Zml zml = cache.get(path);
        if (zml != null && lastModified == zml.getLastModified())
        {//有缓存且时间相同
            return zml;
        }
            
        String filePath = contextPath + path;
        String content = Files.read(filePath, engine.getEncoding());
        zml = new Zml(engine, path, lastModified, content);
        cache.put(path, zml);
        return zml;
    }

    @Override
    public long getLastModified(String path) throws Exception
    {
        if (!Strings.startsWith(path, "/") || !Validates.isMatch(path, engine.getPatterns()))
        {//要求相对于根目录的绝对路径
            throw new Exception("ZML["+path+"]配置不正确，必须/开头且符合配置的模式");
        }
        
        return watcher.getLastModified(path);
    }
    
    @Override
    public void setMaxIdleTime(int maxIdleTime)
    {
        this.cache.setMaxIdleTime(maxIdleTime);
    }
    
    @Override
    public void setMaxKeepTime(int maxKeepTime)
    {
        this.cache.setMaxKeepTime(maxKeepTime);
    }
}
