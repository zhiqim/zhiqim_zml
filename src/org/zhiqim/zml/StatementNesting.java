/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import java.util.List;

import org.zhiqim.kernel.constants.SignConstants;
import org.zhiqim.zml.exception.StatementException;
import org.zhiqim.zml.statement._For;
import org.zhiqim.zml.statement._Function;
import org.zhiqim.zml.statement._If;

/**
 * ZML和嵌套语句解析接口，在解析时需实现该接口
 * @see Zml         ZML本身支持嵌套，内部有_If,_For或者_Function语句
 * @see _If         判断语句内部会嵌套_If,_For
 * @see _For        循环语句内部会嵌套_If,_For
 * @see _Function   全局函数定义语句内部会嵌套_If,_For和_Function
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public interface StatementNesting extends ZmlConstants, SignConstants
{
    /**
     * 获取语句中对应的ZML
     * 
     * @return ZML
     */
    public Zml getZml();
    
    /**
     * 获取对应ZML的路径
     * 
     * @return String
     */
    public String getPath();
    
    /**
     * 获取ZML中的行列索引表
     * 
     * @return List<ZmlLineIndex>
     */
    public List<ZmlLineIndex> getIndexList();
    
    /**
     * 获取需要处理的ZML或语句内容
     * 
     * @return String
     */
    public String getContent();
    
    /**
     * 获取语句/ZML内容对应ZML起始索引号
     * 
     * @return int
     */
    public int getContentBeginIndex();
    
    /** 
     * 获取上一个语句内容 
     * 
     * @param stmt  当前语句
     */
    public String getPrevStatement(Statement stmt) throws StatementException;
    
    /** 
     * 获取下一个语句内容 
     * 
     * @param stmt  当前语句
     */
    public String getNextStatement(Statement stmt) throws StatementException;
}
