/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Vector;

import org.zhiqim.kernel.model.maps.HashMapSC;
import org.zhiqim.kernel.model.maps.HashMapSO;

/**
 * ZhiqimML预定义：
 * 1、预定义的JAVA核心类表
 * 2、预定义的系统变量表
 *
 * @version v1.0.0 @author zouzhigang 2016-12-23 新建与整理
 */
public class ZmlPredefinded
{
    private static final HashMapSC javaMap = new HashMapSC();
    private static final HashMapSO systemMap = new HashMapSO();
    
    static
    {
        //java.lang.*
        javaMap.put("Object", Object.class);
        javaMap.put("Boolean", Boolean.class);
        javaMap.put("Byte", Byte.class);
        javaMap.put("Character", Character.class);
        javaMap.put("Short", Short.class);
        javaMap.put("Integer", Integer.class);
        javaMap.put("Long", Long.class);
        javaMap.put("Float", Float.class);
        javaMap.put("Double", Double.class);
        javaMap.put("BigInteger", BigInteger.class);
        javaMap.put("BigDecimal", BigDecimal.class);
        javaMap.put("Class", Class.class);
        javaMap.put("String", String.class);
        javaMap.put("StringBuffer", StringBuffer.class);
        javaMap.put("StringBuilder", StringBuilder.class);
        javaMap.put("System", System.class);
        javaMap.put("Thread", Thread.class);
        javaMap.put("Runtime", Runtime.class);
        javaMap.put("Process", Process.class);
        
        //java.util.*
        javaMap.put("ArrayList", ArrayList.class);
        javaMap.put("LinkedList", LinkedList.class);
        javaMap.put("Vector", Vector.class);
        javaMap.put("HashMap", HashMap.class);
        javaMap.put("HashSet", HashSet.class);
        javaMap.put("Hashtable", Hashtable.class);
        javaMap.put("LinkedHashMap", LinkedHashMap.class);
        javaMap.put("LinkedHashSet", LinkedHashSet.class);
        javaMap.put("Calendar", Calendar.class);
        javaMap.put("Date", Date.class);
        javaMap.put("Random", Random.class);
    }
    
    /** 获取Java预定义简称表 */
    public static HashMapSC getJavaMap()
    {
        return javaMap;
    }
    
    /** 获取Java预定义简称表 */
    public static Class<?> getJavaClass(String className)
    {
        return javaMap.get(className);
    }
    
    /** 获取系统变量表 */
    public static HashMapSO getSystemMap()
    {
        return systemMap;
    }
    
    /** 获取系统变量 */
    public static Object getSystemVariable(String key)
    {
        return ZmlVariable.chkDynamicVar(systemMap.get(key));
    }
    
    /** 增加系统变量 */
    public static void addSystemVariable(String key, Object value)
    {
        systemMap.put(key, value);
    }
}
