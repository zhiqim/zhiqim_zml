/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import java.util.Iterator;

import org.zhiqim.kernel.model.maps.HashMapSV;
import org.zhiqim.kernel.schedule.Interval;
import org.zhiqim.kernel.schedule.Task;

/**
 * ZML缓存
 *
 * @version v1.0.0 @author zouzhigang 2017-11-21 新建与整理
 */
public class ZmlCache implements Task
{
    private final HashMapSV<Zml> cache = new HashMapSV<>();
    
    private int maxIdleTime;//最长空闲时长，单位秒
    private int maxKeepTime;//最长保持时长，单位秒
    
    public ZmlCache(int maxIdleTime, int maxKeepTime)
    {
        this.maxIdleTime = maxIdleTime;
        this.maxKeepTime = maxKeepTime;
        
        //每15分钟检查一次
        Interval.shedule(this, 15 * 60 * 1000);
    }
    
    public Zml get(String path)
    {
        synchronized (cache)
        {
            Zml zml = cache.get(path);
            if (zml != null)
                zml.setLastAccessed();
            
            return zml;
        }
    }
    
    public void put(String path, Zml zml)
    {
        synchronized (cache)
        {
            if (cache.containsKey(path))
                cache.remove(path);
            
            cache.put(path, zml);
        }
    }
    
    public void remove(String path)
    {
        synchronized (cache)
        {
            cache.remove(path);
        }
    }

    @Override
    public void execute()
    {
        boolean hasMaxIdleTime = maxIdleTime > 0;
        boolean hasMaxKeepTime = maxKeepTime > 0;
        
        if (!hasMaxIdleTime && !hasMaxKeepTime)
            return;
        
        long lastKeepTimeMs = System.currentTimeMillis() - maxKeepTime * 1000;
        long lastIdleTimeMs = System.currentTimeMillis() - maxIdleTime * 1000;
        
        synchronized (cache)
        {
            for (Iterator<Zml> it=cache.values().iterator();it.hasNext();)
            {
                Zml zml = it.next();
                long lastAccessed = zml.getLastAccessed();
                if ((hasMaxKeepTime && lastAccessed < lastKeepTimeMs) 
                  || (hasMaxIdleTime && lastAccessed < lastIdleTimeMs))
                {//超时删除
                    it.remove();
                }
            }
        }
    }
    
    public int getMaxIdleTime()
    {
        return maxIdleTime;
    }

    public int getMaxKeepTime()
    {
        return maxKeepTime;
    }

    public void setMaxIdleTime(int maxIdleTime)
    {
        this.maxIdleTime = maxIdleTime;
    }
    
    public void setMaxKeepTime(int maxKeepTime)
    {
        this.maxKeepTime = maxKeepTime;
    }
}
