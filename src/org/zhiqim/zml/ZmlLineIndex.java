/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

/**
 * ZhiqimML行索引，定义每行对应的索引
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class ZmlLineIndex
{
    private int lineNo;
    
    private int beginIndex;
    private int endIndex;
    
    /** 指定总索引号，判断是否在该行，如果不在返回-1，否则返在该行号 */
    public int getLineNo(int ind)
    {
        if (ind < beginIndex || ind > endIndex)
            return -1;
        else
            return lineNo + 1;
    }
    
    /** 指定总索引号，判断是否在该行，如果不在返回-1，否则返在该行对应的列号 */
    public int getColumnNo(int ind)
    {
        if (ind < beginIndex || ind > endIndex)
            return -1;
        
        return ind - beginIndex + 1;
    }

    public int getBeginIndex()
    {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex)
    {
        this.beginIndex = beginIndex;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    public void setEndIndex(int endIndex)
    {
        this.endIndex = endIndex;
    }

    public int getLineNo()
    {
        return lineNo;
    }

    public void setLineNo(int lineNo)
    {
        this.lineNo = lineNo;
    }
}
