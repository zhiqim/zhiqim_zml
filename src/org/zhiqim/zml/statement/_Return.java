/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.statement;

import org.zhiqim.kernel.util.Strings;
import org.zhiqim.kernel.util.Validates;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ExpressionParser;
import org.zhiqim.zml.Statement;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.exception.StatementException;

/**
 * 执行结束返回，函数中只退出函数，非函数结束执行， 格式为：<#return/>
 * @see _Break      退出循环
 * @see _Continue   继续下一循环
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Return extends Statement
{
    private Expression expression;
    
    @Override
    public boolean isNesting()
    {
        return false;
    }
    
    @Override
    public void parseStatement() throws StatementException
    {
        String s = Strings.trim(getStatement(), RETURN_BEGIN, RETURN_END);
        if (Validates.isEmptyBlank(s))
            return;
        
        try
        {//有表达式
            expression = ExpressionParser.parseExpression(s);
        }
        catch (ExpressionException e)
        {
            throw new StatementException(this, RETURN + e.getMessage());
        }
    }
    
    @Override
    public String process(ZmlVariable variableMap) throws StatementException
    {//无需返回值，则作类型判断
        return null;
    }
    
    /**
     * 生成返回值
     * 
     * @param variableMap           变量表
     * @return                      函数结果字符串
     * @throws StatementException   语句异常
     */
    public Object build(ZmlVariable variableMap) throws StatementException
    {
        try
        {
            return (expression==null)?null:expression.build(variableMap);
        }
        catch(Exception e)
        {
            throw new StatementException(this, CALL + e.getMessage(), e);
        }
    }
    
    /**
     * 返回异常，表明调用于遇到<#return>语句，Zml和_Function会捕捉
     *
     * @version v1.0.0 @author zouzhigang 2016-11-18 新建与整理
     */
    public static class ReturnException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
        private Object value = null;
        
        public ReturnException()
        {
        }
        
        public ReturnException(Object value)
        {
            this.value = value;
        }
        
        public Object getValue()
        {
            return value;
        }
        
        public boolean hasValue()
        {
            return value != null;
        }
    }
}