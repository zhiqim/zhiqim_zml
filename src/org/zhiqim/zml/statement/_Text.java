/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.statement;

import org.zhiqim.kernel.constants.SignConstants;
import org.zhiqim.zml.Statement;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.StatementException;

/**
 * 纯文本语句，直接打印即可，无需解析
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Text extends Statement implements SignConstants
{
    @Override
    public boolean isNesting()
    {
        return false;
    }
    
    @Override
    public boolean isExclusiveLine()
    {//纯文本没有独占行
        return false;
    }
    
    @Override
    public void parseStatement()
    {
        //noting
    }

    @Override
    public String process(ZmlVariable variableMap) throws StatementException
    {//返回文本内容
        return getStatement();
    }
}
