/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.statement;

import org.zhiqim.kernel.util.Strings;
import org.zhiqim.zml.Expression;
import org.zhiqim.zml.ExpressionParser;
import org.zhiqim.zml.Statement;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.StatementException;

/**
 * 变量定义语句，格式为：<#var i=1/>
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Var extends Statement
{
    private String variableName;
    private Expression _expression;
    
    @Override
    public boolean isNesting()
    {
        return false;
    }
    
    @Override
    public void parseStatement() throws StatementException
    {
        String s = Strings.trim(getStatement(), VAR_BEGIN, VAR_END);
        int ind = s.indexOf("=");
        if (ind == -1)
            throw new StatementException(this, VAR + "未找到赋值符(=)");
        
        variableName = s.substring(0, ind).trim();
        
        try
        {
            String expression = s.substring(ind+1).trim();
            _expression = ExpressionParser.parseExpression(expression);
        }
        catch(Exception e)
        {
            throw new StatementException(this, VAR + e.getMessage());
        }
    }

    @Override
    public String process(ZmlVariable variableMap) throws StatementException
    {
        //定义变量，把生成的新变量放置到变量表中，或覆盖到变量表中
        Object value = null;
        
        try
        {
            value = _expression.build(variableMap);
        }
        catch(Exception e)
        {
            throw new StatementException(this, VAR + e.getMessage(), e);
        }
        
        variableMap.addVariable(variableName, value);
        
        //仅执行不返回文本
        return null;
    }

    public String getVariableName()
    {
        return variableName;
    }
    
    public Expression getExpression()
    {
        return _expression;
    }
}
