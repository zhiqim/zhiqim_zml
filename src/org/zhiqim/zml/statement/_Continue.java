/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.statement;

import org.zhiqim.zml.Statement;
import org.zhiqim.zml.ZmlVariable;
import org.zhiqim.zml.exception.StatementException;

/**
 * 继续下一循环, 格式为：<#continue/>
 * @see _Return     退出函数或执行结束
 * @see _Continue   退出循环
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class _Continue extends Statement
{
    @Override
    public boolean isNesting()
    {
        return false;
    }
    
    @Override
    public void parseStatement() throws StatementException
    {
    }
    
    @Override
    public String process(ZmlVariable variableMap) throws StatementException
    {//无需返回值，则作类型判断
        return null;
    }
    
    public static class ContinueException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;
    }

}