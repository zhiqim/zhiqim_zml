/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.exception;

import org.zhiqim.kernel.util.Validates;
import org.zhiqim.zml.Statement;

/**
 * 定义语句解析异常，可以得到出错的路径、行列号
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class StatementException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    private String path;
    private int lineNo;
    private int columnNo;
    
    public StatementException(Statement s, String message)
    {
        this(s.getPath(), s.getBeginLine(), s.getBeginColumn(), message);
    }
    
    public StatementException(Statement s, String message, Throwable exception)
    {
        this(s.getPath(), s.getBeginLine(), s.getBeginColumn(), message, exception);
    }
    
    public StatementException(String path, int lineNo, int columnNo, String message)
    {
        super(new StringBuilder("[").append(Validates.isEmptyBlank(path)?"自定义ZML":path).append("]").append("[第").append(lineNo).append("行],[第").append(columnNo).append("列][").append(message).append("]").toString());
        
        this.path = path;
        this.lineNo = lineNo;
        this.columnNo = columnNo;
    }
    
    public StatementException(String path, int lineNo, int columnNo, String message, Throwable exception)
    {
        super(new StringBuilder("[").append(Validates.isEmptyBlank(path)?"自定义ZML":path).append("]").append("[第").append(lineNo).append("行],[第").append(columnNo).append("列][").append(message).append("]").toString(), exception);
        
        this.path = path;
        this.lineNo = lineNo;
        this.columnNo = columnNo;
    }
    
    /************************************************/
    //异常时获取文件对应的路径、行号和列号
    /************************************************/

    public String getPath()
    {
        return path;
    }

    public int getLineNo()
    {
        return lineNo;
    }

    public int getColumnNo()
    {
        return columnNo;
    }
}
