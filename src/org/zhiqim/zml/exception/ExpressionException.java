/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml.exception;

/**
 * 定义表达式解析异常
 *
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public class ExpressionException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    /**
     * 断言，传入要断言的判断，为false时抛出非法的参数异常
     * 
     * @param param                 断言为false时抛出非法的参数异常
     * @param exception             异常信息
     * @throws ExpressionException  异常
     */
    public static void asserts(boolean param, String exception) throws ExpressionException
    {
        if (!param){
            throw new ExpressionException(exception);
        }
    }
    
    /**
     * 断言对象不为null，如果为null则抛出非法的参数异常
     * 
     * @param param         参数
     * @param exception     异常信息
     * @throws ExpressionException  异常
     */
    public static void assertNotNull(Object param, String exception) throws ExpressionException
    {
        if (param == null){
            throw new ExpressionException(exception);
        }
    }

    public ExpressionException(String message)
    {
        super(message);
    }
    
    public ExpressionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
