/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import org.zhiqim.kernel.model.maps.LinkedMapSV;
import org.zhiqim.zml.statement._Var;

/**
 * ZhiqimML变量更新通知
 *
 * @version v1.0.0 @author zouzhigang 2016-7-13 新建与整理
 */
public interface ZmlVarNotice
{
    /**
     * 回调更新变量的接口定义
     * 
     * @param zml               ZML对象
     * @param configPath        配置路径
     * @param componentPath     组件路径
     * @param varMap            变量表
     */
    public void doUpdateVariable(Zml zml, String configPath, String componentPath, LinkedMapSV<_Var> varMap);
}
