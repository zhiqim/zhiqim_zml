/*
 * 版权所有 (C) 2015 知启蒙(ZHIQIM) 保留所有权利。[遇见知启蒙，邂逅框架梦]
 * 
 * https://zhiqim.org/project/zhiqim_framework/zhiqim_zml.htm
 *
 * Zhiqim Zml is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package org.zhiqim.zml;

import org.zhiqim.zml.exception.ExpressionException;
import org.zhiqim.zml.expression.Operator;
import org.zhiqim.zml.expression.Primitive;

/**
 * 表达式基类<br><br>
 *  
 * @see Operator 运算符表达式<br>
 * @see Primitive  对象原型表达式<br>
 * @version v1.0.0 @author zouzhigang 2014-3-21 新建与整理
 */
public interface Expression extends ZmlConstants
{
    /** 
     * 强制子类实现toString方法 
     * 
     * @return 表达式原型字符串
     */
    public String toString();
    
    /**
     * 强制子类提供表达式类型，方便switch
     * 
     * @return 表达式int类型
     */
    public int getType();
    
    /**
     * 根据变量列表生成结果值
     * 
     * @param variableMap           变量表
     * @return                      结果值
     * @throws ExpressionException  可能的表达式异常
     */
    public Object build(ZmlVariable variableMap) throws ExpressionException;
}
