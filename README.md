### 什么是“ZML”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZML（Zhiqim Markup Language）即“知启蒙标记语言”，是知启蒙定义的、类似于Java & Javascript语法的语句和表达式，通常和XML/HTML混编在一起形成的一种新的标记语言。ZML力求简单易用，目前支持常用的十二种标记语句和五十种表达式，在知启蒙框架体系中被用来代替JSP。

<br>

### 什么是“ZML引擎”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ZML引擎（ZhiqimZML）即“知启蒙标识语言引擎”，是实现ZML规范的引擎系统。支持两种ZML加载方式（按文件目录和按类路径）、能够在ZML文件改动后立即发现并在触发时重新加载、并提供详细的“表达式合并运算优先级”、“变量多维作用域”和“无障碍访问Java代码”，以及设置上下文作用域的配置模板、对模板进行缓存等特性。

<br>

### “ZML引擎”有哪些优点？ 
---------------------------------------
1、ZhiqimZML仅依赖JDK1.7+和ZhiqimKernel。15年的坚持，值得信赖。<br>
2、ZhiqimZML支持十二种语句和五十种表达式基本覆盖业务操作，是[知启蒙WEB容器](https://gitee.com/zhiqim/zhiqim_httpd)的重要组成部分，在ZhiqimDK家族中占据非常重要的位置。<br>
3、对比国外模板引擎Freemarker和Velocity，ZhiqimZML更有优势，结合[知启蒙微内核](https://gitee.com/zhiqim/zhiqim_kernel)的大量工具类（如Strings/Validatest），使得ZhiqimZML可无障碍访问Java代码，称之为Java的标识语言都不为过。<br>
4、最后**自荐一下**，比Spring更轻量、更齐全的J2EE框架 **zhiqim** 正式开源啦，不服来试用。

<br>

### 最简单的ZML引擎使用方法
---------------------------------------
<table style="border-collapse:collapse; border:1px solid #333; height:400px; width:100%">
<tr>
    <td bgcolor="#e1ffff">
        1、准备好一段符合ZML规格的字符串，和一个变量表，即可解析出结果。<br>
        2、支持全局变量表中@AnAlias注解别名表和预定义Java类名的静态变量和静态方法调用。<br>
        3、详细见org.zhiqim.zml.Zmls静态类中的方法，如下提供的parse方法。
    </td>
</tr>
<tr><td>
<img src="https://zhiqim.org/project/images/181507_d7a655a3_2103954.png">
</td>
</tr>
</table>

<br>

### ZML支持的十二种语句&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.zhiqim.com/gitcan/zhiqim/zhiqim_zml/tutorial/primitive.htm">前往五十种表达式</a>
---------------------------------------
一、变量定义语句（_Var）<br>
```
<#var name = "知启蒙模板引擎"/>

表示定义变量name的值为“知启蒙模板引擎”。
``` 
二、函数定义语句（_Function）
```
<#function functionName(param1, param2)>
    <div>param1 = ${param1}</div>
    <div>param2 = ${param2}</div>
</#function>
``` 
三、文件包含语句（_Include）
```
<#include "/zml/inc.zml"/>

表示包含文件“/zml/inc.zml”的内容到该页面中。
```
四、判断语句（_If）
```
<#if (!Validates.isEmpty(name) && name.startWith("知启蒙"))>
    ${name}
</#if>
```
五、遍历语句（_For）
```
<#for (var item : list)>
    <#if (Validates.isNotEmpty(item))>
        ${item}
    </#if>
</#for>

简写：

<#for item : list>
    <#if (Validates.isNotEmpty(item))>
        ${item}
    </#if>
</#for>
```
六、拦截器语句（_Interceptor）
```
<#interceptor "chkLogin"/>

拦截器语句用于在执行之前判断是否支持定义的参数的值，如"chkLogin"表示是一个拦截器名称
```
七、显示结果语句（_Echo）
```
显示结果语句是通过连接、计算和合并的方式执行多个表达式，得到执行结果，然后把结果显示在页面上，值为（null）不显示结果，如：

<#var name = "知启蒙"/>

${name} 表示执行表达式name，得到结果“知启蒙”

${name + "引擎"} 表示执行表达式name + "引擎",得到结果“知启蒙引擎”

${functionName(name, "edf")} 表示调用函数并显示结果

${Strings.trimLeft(name, "知")} 表示类Strings的trimLeft方法，显示结果“启蒙”
```
八、显示格式化结果语句（_Format）
```
显示格式化结果语句是通过连接、计算和合并的方式执行多个表达式，得到执行结果，值为（null）不显示结果，<br>
并对其他Zmls.format(zml)格式化，对<,>,\",\',${,@{,#{格式化，保证浏览器解释成字符串而不是ZML代码，如：

<#var name = "知${abc}启蒙"/>
#{name} 表示执行表达式name，得到结果是“知&#x24;{abc}启蒙”，$符号转为&#x24

#{name + "引\"擎"} 表示执行表达式name + "引\"擎"，得到结果“知&#x24;{abc}启蒙引&quot;擎”，$符号转为&#x24，引号变成&quot;

#{functionName(name, "edf")} 表示调用函数并显示结果

#{Strings.trimLeft(name, "知")} 表示类Strings的trimLeft方法，显示结果“&#x24;{abc}启蒙”
```
九、调用不显示结果语句（_Call）
```
调用语句是通过连接、计算和合并的方式执行多个表达式，执行，但不显示结果到页面上，如：

@{System.out.println("显示信息到控制台，不是显示到页面")} 表示执行System.out.println();
```
十、返回语句（_Return）
```
返回语句应用于标识当前页执行结束，直接返回，和函数function中返回值：

<#if Validates.isEmpty(sessionKey)>
    <#return/>
</#if>

<#function functionName(param1, param2)>
    <#var ret = param1+param2/>
    <#return ret/>
</#function>
```
十一、继续语句（_Continue）
```
继续语句应用于_For语句中，表示结束当次处理，继续下一次：

<#for item : list>
    <#if (Validates.isNotEmpty(item))>
        <#continue/>
    </#if>
</#for>
```
十二、中断语句（_Break）
```
<#for item : list>
    <#if (Validates.isNotEmpty(item))>
        <#break/>
    </#if>
</#for>
```

<br>

### 常用的ZML页面和ZML配置模板
---------------------------------------
![常用的ZML页面](https://zhiqim.org/project/images/202448_e96bd2c6_2103954.png "zhiqim_zml_readme_2.png")
![常用的ZML配置模板](https://zhiqim.org/project/images/2018/0907/202500_0e1e767c_2103954.png "zhiqim_zml_readme_3.png")
<br>
<br>

### 强大的ZML引擎解析文件
---------------------------------------
<table style="border-collapse:collapse; border:1px solid #333; height:400px; width:100%">
<tr>
    <td colspan="3" class="z-bg-cyan">
    1、支持对文件目录/类路径加载配置定义的变量和函数到上下文变量表中，支持根据path查找ZML文件加载。<br>
    2、支持多个组件加载器，依次查找组件配置模板中定义的变量和函数到上下文变量表，和根据path查找的ZML文件加载。<br>
    3、支持自定义设置全局变量。<br>
    4、支持对ZML文件监视，有变动立即知晓，并在被触发时加载文件，如是配置模板会回调通知。<br>
    5、支持对ZML文件缓存，并提供缓存参数maxIdleTime/maxKeepTime。
    </td>
</tr>
<tr bgcolor="#f5f5f5">
    <td width="16%">参数</td>
    <td width="30%">参数类型</td>
    <td width="*">描述</td>
</tr>
<tr>
    <td>notice</td>
    <td>ZmlVarNotice</td>
    <td>设置模板变量变动通知，当/conf/config.zml有变动并触发时回调doUpdate方法</td>
</tr>
<tr>
    <td>encoding</td>
    <td>String</td>
    <td>模板加载编码</td>
</tr>
<tr>
    <td>maxIdleTime</td>
    <td>int</td>
    <td>模板最大空闲时长，建议1小时</td>
</tr>
<tr>
    <td>maxKeepTime</td>
    <td>int</td>
    <td>模板最大保持时长，建议24小时</td>
</tr>
<tr>
    <td>isAscQuery</td>
    <td>boolean</td>
    <td>当有组件配置模板时，是否按顺序查找</td>
</tr>
<tr>
    <td>patterns</td>
    <td>String</td>
    <td>模板匹配模式，默认*.zml,*.htm</td>
</tr>
<tr>
    <td>loader</td>
    <td>ClassZmlLoader<br>FileZmlLoader</td>
    <td>设置根配置模板加载器，类路径/目录</td>
</tr>
<tr>
    <td>cLoaderMap</td>
    <td>ClassZmlLoader</td>
    <td>组件配置模板加载器，类路径/目录，支持多个，<br>通过addComponentZmlLoader()添加</td>
</tr>
<tr>
    <td>globalMap</td>
    <td>HashMap</td>
    <td>全局变量表，通过addGlobalVariable添加</td>
</tr>
<tr><td colspan="3">
<span class="z-text-prewrap z-code zi-px16">

```
//先创建ZML引擎，可以保存起来
ZmlEngine engine = new ZmlEngine();
engine.setFileZmlLoader(new File("./resource"));
engine.setConfigZml("/conf/config.zml");

//调用和解析ZML文件
Zml zml = engine.getZml("/zml/abc.zml");
HashMapSO variableMap = new HashMapSO();
variableMap.put("abc", "知启蒙");
variableMap.put("def", "标识语言");
variableMap.put("isAbc", false);

String result = Zmls.parese(zml, variableMap);
```
</span>
</td>
</tr>
</table>

<br>

### 知启蒙技术框架与交流
---------------------------------------
![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多知启蒙标识语言，[【请戳这里】](https://zhiqim.org/project/zhiqim_framework/zhiqim_zml/tutorial/index.htm)